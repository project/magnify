<?php

namespace Drupal\magnify\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatterBase;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of 'magnify_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "magnify_field_formatter",
 *   label = @Translation("Magnify Image Viewer"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class MagnifyFieldFormatter extends ImageFormatterBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The image style entity storage.
   *
   * @var \Drupal\image\ImageStyleStorageInterface
   */
  protected $imageStyleStorage;

  /**
   * Constructs an ImageFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Thrown if the entity type doesn't exist.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Thrown if the storage handler couldn't be loaded.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, AccountInterface $current_user, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
    $this->imageStyleStorage = $this->entityTypeManager->getStorage("image_style");
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
          $plugin_id,
          $plugin_definition,
          $configuration["field_definition"],
          $configuration["settings"],
          $configuration["label"],
          $configuration["view_mode"],
          $configuration["third_party_settings"],
          $container->get("current_user"),
          $container->get("entity_type.manager"),
      );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'image_style' => '',
      'size' => '',
      'zoom' => '',

    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $image_styles = image_style_options(FALSE);
    $description_link = Link::fromTextAndUrl(
          $this->t("Configure Image Styles"),
          Url::fromRoute('entity.image_style.collection')
      );

    $element['image_style'] = [
      '#title' => $this->t("Image style"),
      '#type' => 'select',
      '#default_value' => $this->getSetting('image_style'),
      '#empty_option' => $this->t("None (original image)"),
      '#options' => $image_styles,
      '#description' => $description_link->toRenderable() + [
        '#access' => $this->currentUser->hasPermission('administer image styles'),
      ],
    ];

    $mode = [
      '90' => $this->t('90x90'),
      '100' => $this->t("100x100"),
      '150' => $this->t("150x150"),
      '250' => $this->t("250x250"),
      '300' => $this->t("300x300"),
    ];

    $element['size'] = [
      '#title' => $this->t("Loop Size"),
      '#type' => 'select',
      '#default_value' => $this->getSetting('size'),
      '#options' => $mode,
    ];

    $modezoom = [
      '1' => $this->t('level 1'),
      '2' => $this->t('level 2'),
      '3' => $this->t('level 3'),
      '4' => $this->t('level 4'),
      '5' => $this->t('level 5'),
    ];

    $element['zoom'] = [
      '#title' => $this->t("zoom scale"),
      '#type' => 'select',
      '#default_value' => $this->getSetting('zoom'),
      '#options' => $modezoom,
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $image_styles = image_style_options(FALSE);
    unset($image_styles['']);
    $image_style_setting = $this->getSetting('image_style');
    if (isset($image_styles[$image_style_setting])) {
      $summary[] = $this->t("Image style: @style", ['@style' => $image_styles[$image_style_setting]]);
    }
    else {
      $summary[] = $this->t("Original image");
    }

    $size = $this->getSetting('size');
    if (isset($size)) {
      $summary[] = $this->t("Loop Size: @size_effect", [
        "@size_effect" => $size,
      ]);
    }

    $zoom = $this->getSetting('zoom');
    if (isset($zoom)) {
      $summary[] = $this->t("zoom Size: @zoom_effect", [
        "@zoom_effect" => $zoom,
      ]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    if (empty($files = $this->getEntitiesToView($items, $langcode)) || !$this->isMagnifyDisplay()) {
      // Early opt-out if the field is empty.
      return $elements;
    }

    $image_style_setting = $this->getSetting("image_style");
    $image_style = NULL;

    if (!empty($image_style_setting)) {
      $image_style = $this->entityTypeManager->getStorage("image_style")->load($image_style_setting);
    }

    $image_uri_values = [];

    foreach ($files as $file) {
      $image_uri = $file->getFileUri();
      // Get image style URL.
      if ($image_style) {
        $image_uri = $image_style->buildUrl($image_uri);
      }
      else {
        // Get absolute path for original image.
        $image_uri = $file->createFileUrl(FALSE);
      }
      // Populate image uri's with file id.
      $fid = $file->id();
      $image_uri_values[$fid] = ["uri" => $image_uri];
    }

    // Populate the title and alt of images based on fid.
    $field_name = $this->fieldDefinition->getName();

    // Get the Entity value as array.
    $node = $items->getEntity();

    if ($node instanceof NodeInterface) {
      $arrayNode = $node->toArray();
      if (array_key_exists($field_name, $arrayNode)) {
        foreach ($arrayNode[$field_name] as $value) {
          $image_uri_values[$value["target_id"]]["alt"] = $value["alt"];
          $image_uri_values[$value["target_id"]]["title"] = $value["title"];
        }
      }
    }

    $size = $this->getSetting('size');
    $zoom = $this->getSetting('zoom');

    $elements[] = [
      '#theme' => 'magnify',
      '#url' => $image_uri_values,
      '#size' => $size,
      '#zoom' => $zoom,
    ];

    // Attach the image field slide show library.
    $elements['#attached']['library'][] = 'magnify/magnify';

    return $elements;
  }

  /**
   * Check if used plugin is  magnify_field_formatter.
   */
  protected function isMagnifyDisplay() {
    return $this->getPluginId() == "magnify_field_formatter";
  }

}
