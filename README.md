# Magnify Image Viewer

## CONTENTS OF THIS FILE

 - Introduction
 - Requirements
 - Installation
 - Configuration
 - Maintainers

## INTRODUCTION

This is a tiny image zoom module that makes it easy to enable intuitive 
image magnification with hover previews.
You can specify any zoom factor, customize the magnifying glass graphic,
and display the zoomed image in a separate container for flexibility.

## REQUIREMENTS

This module dependency on jquery_ui <https://www.drupal.org/project/jquery_ui>


## INSTALLATION

- Download and Enable the module similar to other drupal module.
- Visit any Image fields in Display settings, 
- you will be able to find the "Magnify Image Viewer"
- Select this one and you would be able to select image styles and other config.
- Example: admin/structure/types/manage/page/display

## CONFIGURATION

- Visit any image fields display settings, you will be able to find
the Magnify Image Viewer formatter, select this one and one can also
select image styles.
Ex: admin/structure/types/manage/<content-type-machine-name>/display
- Have the image field settings "Allowed number of values"
to unlimited / limited(more than 1 image).
- Have a custom image style defined under
<http://d10.local/admin/config/media/image-styles>
- Add content & upload more than 1 images to the node and Save..
On node view, Image Compare Viewer will appear for that image field.


## MAINTAINERS

 - Ghazali Mustapha (g.mustapha) - <https://www.drupal.org/u/gmustapha>
