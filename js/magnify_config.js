(function ($, Drupal, drupalSettings) {
  $(document).ready(function () {
    let magnifyElement = document.getElementsByClassName("magnify-img")
    let magnifyLength = magnifyElement.length;
    for (let i = 0; i < magnifyLength; i++) {
      let magnifyId = magnifyElement[i].id;
      $("#"+magnifyId).magnify({
        "width" : magnifyElement[i].dataset.size,
        "height" : magnifyElement[i].dataset.size,
        "scale" : magnifyElement[i].dataset.zoom
      });
    }
  });
})(jQuery, Drupal, drupalSettings);